APP_NAME := simplek8sapp

.DEFAULT_GOAL := help


HELP_FUN = \
	%help; while(<>){push@{$$help{$$2//'targets'}},[$$1,$$3] \
	if/^([\w-_]+)\s*:.*\#\#(?:@(\w+))?\s(.*)$$/}; \
	print"$$_:\n", map"  $$_->[0]".(" "x(24-length($$_->[0])))."$$_->[1]\n",\
	@{$$help{$$_}},"\n" for keys %help; \

help: ##@misc Show this help
	@echo "Usage: make [target] ...\n"
	@perl -e '$(HELP_FUN)' $(MAKEFILE_LIST)


build_image: ##@docker Build docker image
	@echo "Building docker image"
	DOCKER_BUILDKIT=1 docker build -f Dockerfile -t $(APP_NAME):latest .

run_in_docker_compose: ##@docker Run dev env in docker compose
	@echo "Launching local dev environment with docker-compose"
	DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose -f docker-compose.yaml up

deploy_manifests: ##@deploy Deploy to kind using yaml recipes
	@echo "Deploying image manually"
	kind load docker-image $(APP_NAME):latest
	kubectl apply -f deployment.yaml
	kubectl rollout restart deployment/$(APP_NAME)

deploy_helmchart: ##@deploy Deploy to kind using helmchart 
	@echo "Deploying helmchart"
	kind load docker-image $(APP_NAME):latest
	helm upgrade --install $(APP_NAME) helmchart/ --set force_deploy="yes"


.PHONY: help build_image deploy_manifests deploy_helmchart run_in_docker_compose clean_pycache
