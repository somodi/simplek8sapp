Python app deploy in k8s
========================

## Introduction
This is a sample deployment of a tiny python app to kubernetes


## Set up local environment

This project uses docker, docker-compose, KinD, kubectl and helm

You may follow the appropriate installing instructions for your SO/distro.

e.g. for Ubuntu: 
- Docker: https://docs.docker.com/engine/install/ubuntu/
- Docker compose: https://docs.docker.com/compose/install/compose-plugin/
- KinD: https://kind.sigs.k8s.io/docs/user/quick-start/#installation
- kubectl: https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
- helm: https://helm.sh/docs/intro/install/

To ease the task, the script `tools/bootstrap_local_env.sh` will help raise a local development cluster (tested only in Ubuntu 20.04).

It will: 
- Download kind, kubectl and helm
- Install them in `~/.local/bin`
- Create a kind cluster and bind port 80 from host to cluster
- Deploy the nginx-ingress-controller

## Usage

You will find a Makefile with the different recipes provided:

```
$ make
Usage: make [target] ...

docker:
  build_image             Build docker image
  run_in_docker_compose   Run dev env in docker compose
                          
deploy:
  deploy_manifests        Deploy to kind using yaml recipes
  deploy_helmchart        Deploy to kind using helmchart 
                          
misc:
  help                    Show this help

```

With `make build_image` and `make deploy_helmchart` the application will build and deploy.

The application will be accesible on port 80 of the host machine.
It'll answer requests for the hostname 127.0.0.1.nip.io

Going to http://127.0.0.1.nip.io/ you should see the "Hello World"

If you need to adjust this setting to something different please check the `host` variable in `helmchart/values.yaml`


## Pending items

- A real test stage to be run in CI pipeline
- Add native https support
- Migrate bootstrap cluster script to Ansible recipes

## References and inspiration

The project skeleton is inspired in the following resources:


- https://fastapi.tiangolo.com/es/advanced/settings/
- https://fastapi.tiangolo.com/tutorial/bigger-applications/
- https://betterdatascience.com/python-yaml-configuration-files/
- https://blog.atulr.com/docker-local-production-image/
- https://gabnotes.org/lighten-your-python-image-docker-multi-stage-builds/
- https://betterprogramming.pub/sharing-cached-layer-between-docker-and-docker-compose-builds-c2e5f751cee4
- https://kind.sigs.k8s.io/docs/user/ingress/
- https://banzaicloud.com/blog/kind-ingress/
- https://dustinspecker.com/posts/test-ingress-in-kind/
- https://helm.sh/docs/
- https://stackoverflow.com/questions/8889035/how-to-document-a-makefile
- https://www.adictosaltrabajo.com/2020/06/19/primeros-pasos-con-terraform-crear-instancia-ec2-en-aws/