#!/usr/bin/env bash

set -e

BIN_DIR=~/.local/bin

PATH=$PATH:$BIN_DIR

TMP_DIR=/tmp



main() {
    #Ask for confirmation if running interactively
    if [ -t 1 ] ; then 
        ask_for_confirmation
    fi
    check_root_to_install_docker
    download_kind
    download_kubectl
    download_helm
    generate_kind_config
    create_kind_cluster
    generate_kubeconfig
    install_ingress_controller
}


ask_for_confirmation() {
    echo ""
    echo "This script will install docker, docker-compose kind, kubectl and helm"
    echo "It will also populate the kubeconfig in ~/.kube/config if not present"
    echo "The binaries will be copied to ~/.local/bin"
    echo "You will need ~100MB in your home and the same size in /tmp/ for the download"
    echo ""
    echo "After that, it will create a kind cluster and deploy nginx-ingress controller"
    echo ""
    while true; do
        read -p "Please confirm if you want to continue (y/n)" yn
        case $yn in
            [Yy]* ) 
                break
                ;;
            [Nn]* ) 
                exit
                ;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}

check_root_to_install_docker() {
    if [ "$EUID" -ne 0 ]; then
        echo ""
        echo "You are not running this script as root"
        echo "We won't be able to install docker and docker compose for you"
        echo "If you already have both packages installed we may try to proceed"
        echo ""
        while true; do
            read -p "Please confirm that you have docker and docker compose installed (y/n)" yn
            case $yn in
                [Yy]* ) 
                    break
                    ;;
                [Nn]* ) 
                    exit
                    ;;
                * ) echo "Please answer yes or no.";;
            esac
        done
    else
        install_docker
    fi
}

install_docker() {
    apt update
    apt install -y docker docker-compose
}

download_kind() {

    KIND_DOWNLOAD_URL=https://kind.sigs.k8s.io/dl/v0.14.0/kind-linux-amd64

    echo "Downloading kind binary"
    curl -Lo $TMP_DIR/kind $KIND_DOWNLOAD_URL
    chmod +x $TMP_DIR/kind
    mkdir -p $BIN_DIR
    cp $TMP_DIR/kind $BIN_DIR/kind
} 

download_kubectl() {

    KUBECTL_VERSION=$(curl -L -s https://dl.k8s.io/release/stable.txt)
    KUBECTL_DOWNLOAD_URL="https://dl.k8s.io/release/$KUBECTL_VERSION/bin/linux/amd64/kubectl"
    KUBECTL_SIGNATURE_URL="https://dl.k8s.io/$KUBECTL_VERSION/bin/linux/amd64/kubectl.sha256"

    echo "Downloading kubectl version $KUBECTL_VERSION"
    curl -Lo $TMP_DIR/kubectl $KUBECTL_DOWNLOAD_URL
    curl -Lo $TMP_DIR/kubectl.sha256 $KUBECTL_SIGNATURE_URL
    pushd $TMP_DIR 
    echo $(cat kubectl.sha256) kubectl | sha256sum --check
    popd
    chmod +x $TMP_DIR/kubectl
    mkdir -p $BIN_DIR
    cp $TMP_DIR/kubectl $BIN_DIR/kubectl
} 

download_helm() {

    HELM_VERSION=$(curl -s https://api.github.com/repos/helm/helm/releases/latest | grep "tag_name" | cut -d \" -f 4 )
    HELM_DOWNLOAD_URL="https://get.helm.sh/helm-$HELM_VERSION-linux-amd64.tar.gz"
    HELM_SIGNATURE_URL="https://get.helm.sh/helm-$HELM_VERSION-linux-amd64.tar.gz.sha256sum"

    echo "Downloading helm version $HELM_VERSION"
    curl -Lo $TMP_DIR/helm.tar.gz $HELM_DOWNLOAD_URL
    echo "$HELM_SIGNATURE_URL"
    curl $HELM_SIGNATURE_URL | cut -d " " -f 1 > $TMP_DIR/helm.sha256
    pushd $TMP_DIR
    echo $(cat helm.sha256) helm.tar.gz | sha256sum --check
    popd
    tar xf $TMP_DIR/helm.tar.gz --strip-components=1 -C $TMP_DIR linux-amd64/helm
    chmod +x $TMP_DIR/helm
    mkdir -p $BIN_DIR
    cp $TMP_DIR/helm $BIN_DIR/helm
} 

generate_kind_config() {
    echo "Generating KinD cluster config"
    cat >$TMP_DIR/cluster-config.yaml <<EOL
---
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraPortMappings:
  - containerPort: 80
    hostPort: 80
    protocol: TCP

EOL
}

create_kind_cluster() {
    echo "Creating kind cluster for local testing"
    kind create cluster --config=$TMP_DIR/cluster-config.yaml
}

generate_kubeconfig() {
    if [ ! -f ~/.kube/config ]; then
        echo "Populating .kube/config with KinD values"
        mkdir -p ~/.kube/ 
        kind get kubeconfig > ~/.kube/config
    fi
}

install_ingress_controller() {
    echo "Installing ingress controller"
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
}


main "$@"; exit
