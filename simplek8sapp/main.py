import uvicorn

from fastapi import FastAPI

from config import config

bind_ip = config.config["server"]["listen"]["address"]
bind_port = config.config["server"]["listen"]["port"]


app = FastAPI()

@app.get("/{full_path:path}")
async def root():
    return {
        "message": "Hello World"
    }

def main():
    uvicorn.run(app, host=bind_ip, port=bind_port)

if __name__ == '__main__':
    main()
