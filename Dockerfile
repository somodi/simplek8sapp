FROM python:3.8 AS devimage

WORKDIR /opt/simplek8sapp
COPY ./simplek8sapp/requirements.txt ./requirements.txt
RUN pip install -r requirements.txt
# RUN python -m compileall .
COPY ./simplek8sapp/ ./

RUN useradd -ms /bin/bash simplek8sapp
USER simplek8sapp

EXPOSE 8080
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8080", "--reload"]


# Set production settings
FROM devimage AS production

COPY ./simplek8sapp/config-prod.yaml ./config.yaml


CMD ["python", "main.py"]